/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author user
 */

public class Main {

    // declare
    private static FileWriter outputWriter;

    public static void main(String[] args) throws IOException {
        // declare input file reader
        File inputFile = new File("input.txt");
        Scanner inputReader = new Scanner(inputFile);

        // read first line to get number of commands
        int numOfCommands = Integer.parseInt(inputReader.nextLine());

        // initialize output file writer
        outputWriter = new FileWriter("output.txt");

        // this variable is to store the line after read it from file
        String line;
        for (int i = numOfCommands; i > 0; i--) {
            // read next line
            line = inputReader.nextLine();

            // split the line into a command and an input
            String[] words = line.split(" ");
            String command = words[0];
            String input = words[1];

            if (command.equals("charPattern")) {
                char character = input.charAt(0);
                charPattern(character);
                outputWriter.write("\n");
            }
            if (command.equals("drawDiamond")) {
                int size = Integer.parseInt(words[1]);
                drawDiamond(size, 1);
            }
            if (command.equals("bricksToUnload")) {
                int res = bricksToUnload(Integer.parseInt(input));
                outputWriter.write("" + res);
                outputWriter.write("\n");
            }

        }
        outputWriter.flush();
        outputWriter.close();

    }

    private static void charPattern(char character) throws IOException {
        // Stop condition
        if (character == 'A') {
            // print "A"
            outputWriter.write("A");
            return;
        }
        // call for previous character
        charPattern((char) (character - 1));
        // print character
        outputWriter.write(character);
        // call for previous character
        charPattern((char) (character - 1));
    }


    private static void drawDiamond(int size, int line) throws IOException {
        // Stop when reach size * 2 line,
        // if size is n, then we need (n * 2 - 1) lines
        // therefore, we stop at size * 2
        if (line > size * 2) {
            return;
        }
        // spaces is the difference between size and line number
        // it is maximum in the first line, and zero when (line = size) or (at the middle)
        int spaces = Math.abs(size - line);
        String lineStr = "";
        // print spaces
        for (int i = 1; i <= spaces; i++) {
            lineStr += " ";
//            outputWriter.write(" ");
        }
        // print diamond line
        for (int i = 0; i < size - spaces; i++) {
            lineStr += "* ";
//            outputWriter.write("* ");
        }
        lineStr = lineStr.substring(0, lineStr.length() - 1);
        outputWriter.write(lineStr);

        // print an empty line
        outputWriter.write("\n");
        // call the method for the next line
        drawDiamond(size, line + 2);
    }

    private static int bricksToUnload(int bricks) {
        // stop condition 1:
        // this means that we ended up in an accepted solution
        // Therefore, we return 1 to increase the total solutions
        if (bricks == 0) {
            return 1;
        }
        // stop condition 2:
        // this is not a solution, because we couldn't divide bricks correctly
        // and we need more bricks to implement the solution,
        // therefore we return 0
        if (bricks < 0) {
            return 0;
        }
        // we call the same method for the 3 possible ways to unload bricks
        // we take the result of choosing every one of the 3 ways and we take the sum as the total
        // solution for current problem
        int val1 = bricksToUnload(bricks - 1);
        int val2 = bricksToUnload(bricks - 2);
        int val3 = bricksToUnload(bricks - 3);
        // we return the total number of solutions
        int solutions = val1 + val2 + val3;
        return solutions;
    }
}
