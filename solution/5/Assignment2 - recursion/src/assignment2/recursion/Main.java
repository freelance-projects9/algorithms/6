/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author user
 */

public class Main {

    // define input path
    private static String inputPath = "input.txt";

    // define output path
    private static String outputPath = "output.txt";

    // declare file writer
    private static FileWriter writer;

    public static void main(String[] args) throws IOException {

        // initialize file writer
        writer = new FileWriter(outputPath);


        // define input file reader
        File inputFile = new File(inputPath);
        Scanner inputReader = new Scanner(inputFile);

        // read first line to get number of commands
        int numOfCommands = Integer.parseInt(inputReader.nextLine());

        String commandType;
        String commandInput;

        String[] words;
        for (int i = 0; i < numOfCommands; i++) {
            // read next line, and split it to get a string array
            words = inputReader.nextLine().split(" ");
            commandType = words[0];
            commandInput = words[1];
            // execute command
            executeCommand(commandType, commandInput);
        }

        writer.close();
    }

    private static void executeCommand(String commandType, String commandInput) throws IOException {
        switch (commandType) {
            case "drawDiamond":
                // pass size and start from line number 1
                drawDiamond(Integer.parseInt(commandInput), 1);
                return;
            case "bricksToUnload":
                // calculate result
                int result = bricksToUnload(Integer.parseInt(commandInput));
                // write result
                writer.write("" + result);
                writer.write("\n");
                return;
            case "charPattern":
                charPattern(commandInput.charAt(0));
                writer.write("\n");
                return;
            default:
                System.out.println("Error!");
        }
    }

    private static void charPattern(char character) throws IOException {
        // Stop condition
        if (character != 'A') {
            char previousChar = (char) (character - 1);
            // call for previous character
            charPattern(previousChar);
            // print character
            writer.write(character);
            // call for previous character
            charPattern(previousChar);
        } else {
            // print "A"
            writer.write("A");
        }
    }


    private static void drawDiamond(int maxSize, int numOfStars) throws IOException {
        // Stop condition
        if (numOfStars >= maxSize * 2) {
            return;
        }

        // build the line
        String lineStr = "";
        if (numOfStars < maxSize) {
            // build spaces, we need (maxSize - numOfStars) spaces
            lineStr = lineStr + getChars(" ", maxSize - numOfStars);

            // build stars, we need (numOfStars) stars
            lineStr = lineStr + getChars("* ", numOfStars);

        } else {
            // build spaces, we need (numOfStars - maxSize) spaces
            lineStr = lineStr + getChars(" ", numOfStars - maxSize);

            // build stars, we need (maxSize * 2 - numOfStars) stars
            lineStr = lineStr + getChars("* ", maxSize * 2 - numOfStars);
        }
        // remove last char from the line, to remove additional space
        lineStr = lineStr.substring(0, lineStr.length() - 1);

        // write result to the output file
        writer.write(lineStr);
        writer.write("\n");

        // call the method for the next line (numOfStars is more than current with 2)
        int nextLine = numOfStars + 2;
        drawDiamond(maxSize, nextLine);
    }

    // build string, of ch characters with a length of (count)
    private static String getChars(String ch, int count) throws IOException {
        String result = "";
        for (int i = 0; i < count; i++) {
            result += ch;
        }
        return result;
    }

    private static int bricksToUnload(int bricks) {
        // stop condition 1:
        // this means that we ended up in an accepted solution
        // Therefore, we return 1 to increase the total solutions
        if (bricks == 0) {
            return 1;
        }
        // stop condition 2:
        // this is not a solution, because we couldn't divide bricks correctly
        // and we need more bricks to implement the solution,
        // therefore we return 0
        if (bricks < 0) {
            return 0;
        }
        // we call the same method for the 3 possible ways to unload bricks
        // we take the result of choosing every one of the 3 ways and we take the sum as the total
        // solution for current problem
        int val1 = bricksToUnload(bricks - 1);
        int val2 = bricksToUnload(bricks - 2);
        int val3 = bricksToUnload(bricks - 3);
        // we return the total number of solutions
        return val1 + val2 + val3;
    }
}
