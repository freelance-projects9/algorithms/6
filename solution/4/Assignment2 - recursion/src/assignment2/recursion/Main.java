/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 *
 * @author user
 */


public class Main  {

    // define input path
    private static String inputPath = "input.txt";

    // define output path
    private static String outputPath = "output.txt";

    // declare file writer
    private static FileWriter writer;

    public static void main(String[] args) throws IOException {

        // initialize file writer
        writer = new FileWriter(outputPath);

        // read all lines from the input file
        List<String> lines = Files.readAllLines(Paths.get(inputPath));

        // parse first line to get number of commands
        int numOfCommands = Integer.parseInt(lines.get(0));

        String[] words;
        for (int i = 1; i <= numOfCommands; i++) {
            // split each line into a command and an input
            words = lines.get(i).split(" ");
            executeCommand(words[0], words[1]);
        }
        writer.close();
    }

    private static void executeCommand(String commandType, String commandInput) throws IOException {
        switch (commandType) {
            case "drawDiamond":
                drawDiamond(Integer.parseInt(commandInput), 1);
                return;
            case "bricksToUnload":
                // calculate accepted solutions
                int res = bricksToUnload(Integer.parseInt(commandInput));
                // print result to the output file
                writer.write("" + res);
                writer.write("\n");
                return;
            case "charPattern":
                charPattern(commandInput.charAt(0));
                writer.write("\n");
                return;
            default:
                System.out.println("Bad Command!");
        }
    }

    private static void charPattern(char character) throws IOException {
        // Stop condition
        if (character != 'A') {
            // call for previous character
            charPattern((char) (character - 1));
            // print character
            writer.write(character);
            // call for previous character
            charPattern((char) (character - 1));
        } else {
            // print "A"
            writer.write("A");
        }
    }


    private static void drawDiamond(int maxSize, int numOfStars) throws IOException {
        // Stop when reach size * 2,
        if (numOfStars >= maxSize * 2) {
            return;
        }

        // spaces is the difference between max size and required number of stars
        // it is maximum in the first line, and zero when (numOfStars = maxSize) (at the middle)
        int spaces;
        if (numOfStars < maxSize)
            spaces = maxSize - numOfStars;
        else
            spaces = numOfStars - maxSize;

        // build the line
        String lineChars = "";
        // add spaces
        for (int i = 0; i < spaces; i++) {
            lineChars += " ";
        }
        // add stars
        for (int i = 0; i < maxSize - spaces; i++) {
            lineChars += "* ";
        }
        // remove additional space at the end of the line
        lineChars = lineChars.substring(0, lineChars.length() - 1);
        // write line to the file
        writer.write(lineChars);

        // print an empty line
        writer.write("\n");
        // call the method for the next line
        drawDiamond(maxSize, numOfStars + 2);
    }


    private static int bricksToUnload(int bricks) {
        // stop condition 1:
        // this means that we ended up in an accepted solution
        // Therefore, we return 1 to increase the total solutions
        if (bricks == 0) {
            return 1;
        }
        // stop condition 2:
        // this is not a solution, because we couldn't divide bricks correctly
        // and we need more bricks to implement the solution,
        // therefore we return 0
        if (bricks < 0) {
            return 0;
        }
        // we call the same method for the 3 possible ways to unload bricks
        // we take the result of choosing every one of the 3 ways and we take the sum as the total
        // solution for current problem
        int val1 = bricksToUnload(bricks - 1);
        int val2 = bricksToUnload(bricks - 2);
        int val3 = bricksToUnload(bricks - 3);
        // we return the total number of solutions
        return val1 + val2 + val3;
    }
}
