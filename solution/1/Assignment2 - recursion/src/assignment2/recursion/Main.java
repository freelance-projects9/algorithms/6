/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author user
 */

public class Main {

    // declare output writer
    private static FileWriter outputWriter;

    public static void main(String[] args) throws IOException {

        // declare input file reader
        File inputFile = new File("input.txt");
        Scanner inputReader = new Scanner(inputFile);

        // initialize output file writer
        outputWriter = new FileWriter("output.txt");

        // read first line to get number of commands
        int numOfCommands = Integer.parseInt(inputReader.nextLine());

        // this variable is to store the line after read it from file
        String line;
        for (int i = 0; i < numOfCommands; i++) {
            if(!inputReader.hasNextLine()){
                break;
            }
            // read next line
            line = inputReader.nextLine();

            // split the line into a command and an input
            String[] words = line.split(" ");
            String command = words[0];
            String input = words[1];

            switch (command) {
                case "charPattern":
                    outputWriter.write("charPattern:\n");
                    charPattern(input.charAt(0));
                    outputWriter.write("\n\n");
                    break;
                case "drawDiamond":
                    outputWriter.write("drawDiamond:\n");
                    drawDiamond(Integer.parseInt(words[1]), 1);
                    outputWriter.write("\n");
                    break;
                case "bricksToUnload":
                    outputWriter.write("bricksToUnload\n");
                    int res = bricksToUnload(Integer.parseInt(input));
                    outputWriter.write("" + res);
                    outputWriter.write("\n\n");
                    break;
            }
        }
        outputWriter.flush();
        outputWriter.close();
    }

    private static void charPattern(char character) throws IOException {
        // Stop condition
        if (character == 'A') {
            // print "A"
            outputWriter.write("A");
            return;
        }
        // call for previous character
        charPattern((char) (character - 1));
        // print character
        outputWriter.write(character);
        // call for previous character
        charPattern((char) (character - 1));
    }


    private static void drawDiamond(int size, int line) throws IOException {
        // Stop when we reach to the last line, and that is when line == size
        if (line > size) {
            return;
        }

        // spaces is the difference between size and line number
        // it is maximum in the first line, and zero when (line = size / 2) or (at the middle)
        // (line * 2 - 1) is the line-th odd number,
        // if line = 1, => 1
        // if line = 2, => 3
        // if line = 3, => 5
        int spaces = Math.abs(size - (line * 2 - 1));
        // print spaces
        for (int i = 0; i < spaces; i++) {
            outputWriter.write(" ");
        }
        // print diamond line
        // we print stars with space after, except for the last one
        for (int i = 0; i < (size - spaces) - 1; i++) {
            outputWriter.write("* ");
        }
        // we print the last star in the line here
        // if we have only one star, the previous loop won't print anything
        outputWriter.write("*");

        // print an empty line
        outputWriter.write("\n");

        // call the method for the next line
        drawDiamond(size, ++line);
    }

    private static int bricksToUnload(int bricks){
        // case 1:
        // accepted solution, because no bricks are left,
        // and we need no more bricks to apply our solution
        // Therefore, we return 1 to increase the total solutions
        if(bricks == 0){
            return 1;
        }
        // case 2:
        // not accepted, because we need more bricks to apply the chosen solution
        // therefore we return 0
        if (bricks < 0) {
            return 0;
        }
        // we call the same method for the 3 possible ways to unload bricks
        // we take the result of choosing every one of the 3 ways, and we take the sum as the total
        // solution for current problem
        int val1 = bricksToUnload(bricks - 1);
        int val2 = bricksToUnload(bricks - 2);
        int val3 = bricksToUnload(bricks - 3);
        // we return the total number of solutions
        return val1 + val2+ val3;
    }
}
