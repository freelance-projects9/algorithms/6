/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author user
 */

public class Main {

    // file writer
    private static FileWriter fileWriter;
    // scanner
    private static Scanner scanner;
    // We will use this to read lines from the file
    private static String tempLine;

    public static void main(String[] args) throws IOException {

        // initialize file writer
        fileWriter = new FileWriter("output.txt");

        // initialize scanner
        File file = new File("input.txt");
        scanner = new Scanner(file);

        // read first line to get number of commands
        tempLine = scanner.nextLine();
        int commandsCount = Integer.parseInt(tempLine);

        // read commands
        String method;
        String input;
        int k = 0;
        while (k < commandsCount) {
            // read next command
            tempLine = scanner.nextLine();
            // split it to get a string array
            String[] temp = tempLine.split(" ");

            // extract command name and input
            method = temp[0];
            input = temp[1];
            // choose the suitable method to execute command
            chooseMethod(method, input);
            k++;
        }

        fileWriter.close();
    }


    private static void chooseMethod(String method, String input) throws IOException {
        switch (method) {
            case "drawDiamond":
                // parse size of the diamond
                int size = Integer.parseInt(input);
                // pass size, initial stars count and scpaces count
                // the final input is to tell that stars are increasing
                drawDiamond(size, 1, size - 1, true);
                return;
            case "bricksToUnload":
                // calculate result
                int bricks = Integer.parseInt(input);
                int result = bricksToUnload(bricks);
                // write result
                fileWriter.write("" + result);
                fileWriter.write("\n");
                return;
            case "charPattern":
                char ch = input.charAt(0);
                charPattern(ch);
                fileWriter.write("\n");
                return;
        }
    }

    private static void charPattern(char character) throws IOException {
        // Stop condition
        if (character == 'A') {
            // print "A"
            fileWriter.write("A");
            return;
        }
        char previousChar = (char) (character - 1);
        // call for previous character
        charPattern(previousChar);
        // print character
        fileWriter.write(character);
        // call for previous character
        charPattern(previousChar);

    }

    private static void drawDiamond(int size, int stars, int spaces, boolean asc) throws IOException {
        // Stop or continue condition
        // this condition determines if we reach the end of the diamond
        if (stars > 0) {
            // if we reach the middle, stars should be decreasing
            if (stars == size) {
                asc = false;
            }
            // construct line
            String line;
            String spacesStr = "";
            String starsStr = "";
            // build spaces
            for (int i = 0; i < spaces; i++) {
                spacesStr += " ";
            }
            // build stars
            for (int i = 0; i < stars; i++) {
                starsStr += "* ";
            }
            // concatenate spaces and stars
            line = spacesStr + starsStr;
            // remove the last unnecessary space from the line,
            line = line.substring(0, line.length() - 1);

            // write line to the output file
            fileWriter.write(line);
            fileWriter.write("\n");

            // if asc is true, that means we want to increase the stars and decrease the spaces
            if (asc) {
                drawDiamond(size, stars + 2, spaces - 2, true);
            }
            // else we do the opposite, we decrease the stars and increase spaces
            else {
                drawDiamond(size, stars - 2, spaces + 2, false);
            }
        }
    }

    private static int bricksToUnload(int bricks) {
        // unaccepted solution
        if (bricks < 0) {
            return 0;
        }
        // accepted solution
        else if (bricks == 0) {
            return 1;
        }

        // try the 3 different ways to unload bricks
        int v1 = bricksToUnload(bricks - 1);
        int v2 = bricksToUnload(bricks - 2);
        int v3 = bricksToUnload(bricks - 3);
        return  v1 + v2 + v3;
    }
}
