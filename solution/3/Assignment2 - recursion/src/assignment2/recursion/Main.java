/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
/**
 *
 * @author user
 */


public class Main {

    // define input path
    private static String inputPath = "input.txt";

    // define output path
    private static String outputPath = "output.txt";

    // declare print writer
    private static FileWriter writer;


    // this variable is to store the line after reading it from the file
    private static String line;

    public static void main(String[] args) throws IOException {

        // initialize output file writer
        writer = new FileWriter(outputPath);

        // read all lines from the input file
        List<String> lines = Files.readAllLines(Paths.get(inputPath));

        int i = 1;
        String commandType;
        String commandInput;

        do {
            // read next line
            line = lines.get(i);

            // split the line into a command and an input
            String[] words = line.split(" ");
            commandType = words[0];
            commandInput = words[1];
            executeCommand(commandType, commandInput);
            i++;
        } while (i < lines.size());

        writer.close();

    }

    private static void executeCommand(String commandType, String commandInput) throws IOException {
        switch (commandType) {
            case "charPattern":
                // we pass the input as char
                charPattern(commandInput.toCharArray()[0]);
                writer.write("\n");
                break;
            case "drawDiamond":
                // we pass the size and 1 to start from line number 1
                drawDiamond(Integer.parseInt(commandInput), 1);
                break;
            case "bricksToUnload":
                // we get the result as an integer then we print it to the output file
                int res = bricksToUnload(Integer.parseInt(commandInput));
                writer.write("" + res);
                writer.write("\n");
                break;
        }
    }

    private static void charPattern(char character) throws IOException {
        // Stop condition
        if (character != 'A') {
            // call for previous character
            charPattern((char) (character - 1));
            // print character
            writer.write(character);
            // call for previous character
            charPattern((char) (character - 1));
        } else {
            // print "A"
            writer.write("A");
        }
    }


    private static void drawDiamond(int maxLine, int line) throws IOException {
        // Stop when reach line number size * 2 ,
        // if size is n, then we need (n * 2 - 1) lines
        // therefore, we stop at size * 2
        if (line >= maxLine * 2) {
            return;
        }
        // spaces is the difference between size and line number
        // it is maximum in the first line, and zero at the middle, when (line = size)
        int spaces = Math.abs(maxLine - line);

        // print spaces
        for (int i = 0; i < spaces; i++) {
            writer.write(" ");
        }

        // print diamond line
        for (int i = 0; i < maxLine - spaces; i++) {
            writer.write("*");
            // this condition to break with no additional space at the end of the line
            if (i == maxLine - spaces - 1)
                break;
            writer.write(" ");
        }

        // print an empty line
        writer.write("\n");
        // call the method for the next line
        drawDiamond(maxLine, line + 2);
    }


    private static int bricksToUnload(int bricks) {
        // stop condition 1:
        // this means that we ended up in an accepted solution
        // Therefore, we return 1 to increase the total solutions
        if (bricks == 0) {
            return 1;
        }
        // stop condition 2:
        // this is not a solution, because we couldn't divide bricks correctly
        // and we need more bricks to implement the solution,
        // therefore we return 0
        if (bricks < 0) {
            return 0;
        }
        // we call the same method for the 3 possible ways to unload bricks
        // we take the result of choosing every one of the 3 ways, and we take the sum as the total
        // solution for current problem
        int val1 = bricksToUnload(bricks - 1);
        int val2 = bricksToUnload(bricks - 2);
        int val3 = bricksToUnload(bricks - 3);
        // we return the total number of solutions
        return val1 + val2 + val3;
    }
}
