/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author user
 */

public class Main {

    // declare file writer
    private static FileWriter writer;

    // declare scanner
    private static Scanner scanner;

    public static void main(String[] args) throws IOException {

        // initialize file writer
        writer = new FileWriter("output.txt");

        // initialize scanner 
        File inputFile = new File("input.txt");
        scanner = new Scanner(inputFile);

        // read first line
        String line = scanner.nextLine();

        // parse first line to get number of commnds
        int nCommands = Integer.parseInt(line);

        // read commands (the rest of lines in the input file)
        readCommands(nCommands);

        writer.close();
    }

    private static void readCommands(int n) throws IOException {

        int i = 0;
        while (i < n) {
            // read next line (next command)
            String command = scanner.nextLine();
            // split it to get command name and input
            String[] temp = command.split(" ");
            // run command
            execute(temp[0], temp[1]);
            i++;
        }
    }

    private static void execute(String command, String input) throws IOException {
        switch (command) {
            case "drawDiamond":
                // size of the diamond
                int size = Integer.parseInt(input);
                // pass size and start from line number 1 where stars = 1
                drawDiamond(size, 1, 1);
                return;
            case "bricksToUnload":
                // get result from method
                int bricks = Integer.parseInt(input);
                int result = bricksToUnload(bricks);
                // write result
                writer.write("" + result);
                writer.write("\n");
                return;
            case "charPattern":
                charPattern(input.charAt(0));
                writer.write("\n");
                return;
            default:
                System.out.println("Error!");
        }
    }

    private static void charPattern(char character) throws IOException {
        // Stop condition when reach to the 'A' character
        if (character != 'A') {
            char previousChar = (char) (character - 1);
            // repeat method for previous character
            charPattern(previousChar);
            // print current character
            writer.write(character);
            // repeat method for previous character
            charPattern(previousChar);
        } else {
            // print character "A"
            writer.write("A");
        }
    }

    private static void drawDiamond(int size, int stars, int index) throws IOException {
        // Termination
        if (stars < 1) {
            return;
        }
        // build line
        String line = "";
        // add (size - stars) spaces
        for (int i = 0; i < size - stars; i++) {
            line += " ";
        }
        // add stars
        for (int i = 0; i < stars; i++) {
            line += "*";
            // only add a space if it is not the last star in the line
            if (i < stars - 1)
                line += " ";
        }

        // write line to the output file
        writer.write(line);
        writer.write("\n");

        // first case: we have reached the middle of the diamond
        // and started to print the lower part of it --> the stars are decreasing now
        if (index > (size - 1) / 2) {
            drawDiamond(size, stars - 2, index + 1);
        }
        // second case: we are still printing the upper part of the diamond, the stars are increasing
        else {
            drawDiamond(size, stars + 2, index + 1);
        }
    }


    private static int bricksToUnload(int bricks) {
        // first case:
        // we need more bricks to apply the solution, so it is not an accepted solution
        if (bricks < 0) {
            return 0;
        }
        // second case:
        // there are no bricks left, and the solution is accepted
        else if (bricks == 0) {
            return 1;
        }

        // we calculate the possible solutions for the choosing the 3 possible ways to unload bricks
        // then we take the sum (total accepted solutions count) and return it
        return bricksToUnload(bricks - 1) + bricksToUnload(bricks - 2) + bricksToUnload(bricks - 3);
    }
}
