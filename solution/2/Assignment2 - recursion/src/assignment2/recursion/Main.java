/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment2.recursion;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author user
 */

public class Main {

    // define input path
    private static String inputFilePath = "input.txt";

    // define output path
    private static String outputFilePath = "output.txt";

    // declare print writer
    private static PrintWriter writer;

    // this variable is to store the line after reading it from the file
    private static String line;

    // number of commands the file contains
    private static int numOfCommands;

    public static void main(String[] args) throws IOException {

        // get input reader
        Scanner inputReader = getScanner();

        // read first line to get number of commands
        numOfCommands = Integer.parseInt(inputReader.nextLine());

        // initialize output file writer
        writer = new PrintWriter(outputFilePath);

        int i = 0;
        String commandType;
        String commandInput;

        while (i < numOfCommands) {
            if (inputReader.hasNextLine()) {
                // read next line
                line = inputReader.nextLine();

                // split the line into a command and an input
                String[] words = line.split(" ");
                commandType = words[0];
                commandInput = words[1];
                executeCommand(commandType, commandInput);
            }
            i++;

        }
        writer.close();
    }

    private static void executeCommand(String commandType, String commandInput) throws IOException {
        // for each command type we call a method that solve the problem
        // and print the solution to the output file
        if ("charPattern".equals(commandType)) {// we need to pass a char,
            // and because commandInput is a string containing one char we take the first index of it.
            writer.write("charPattern:\n");
            charPattern(commandInput.charAt(0));
            writer.write("\n\n");
        } else if ("drawDiamond".equals(commandType)) {// we pass the size of the diamond, and 1 to start from the first line
            writer.write("drawDiamond:\n");
            drawDiamond(Integer.parseInt(commandInput), 1);
            writer.write("\n");
        } else if ("bricksToUnload".equals(commandType)) {// the method calculates the possible solutions and returns an integer
            writer.write("bricksToUnload\n");
            int res = bricksToUnload(Integer.parseInt(commandInput));
            // we print result to the output file
            writer.write("" + res);
            writer.write("\n\n");
        }
    }

    private static Scanner getScanner() throws FileNotFoundException {
        // declare input file reader
        File inputFile = new File(inputFilePath);
        return new Scanner(inputFile);
    }

    private static void charPattern(char character) throws IOException {
        // Stop condition
        if (character == 'A') {
            // print "A"
            writer.write("A");
            return;
        }
        // call for previous character
        charPattern((char) (character - 1));
        // print character
        writer.write(character);
        // call for previous character
        charPattern((char) (character - 1));
    }


    private static void drawDiamond(int size, int line) throws IOException {
        // Stop when we reach to the last line, and that is when line == size
        if (line > size) {
            return;
        }

        // we calculate spaces count
        int spaces = Math.abs(size - (line * 2 - 1));

        // print spaces
        for (int i = 0; i < spaces; i++) {
            writer.write(" ");
        }

        // print diamond line
        for (int i = 0; i < (size - spaces) - 1; i++) {
            writer.write("* ");
        }
        writer.write("*\n");

        // call the method for the next line
        drawDiamond(size, ++line);
    }

    private static int bricksToUnload(int bricks) {
        // stop condition 1:
        // this means that we ended up in an accepted solution
        // Therefore, we return 1 to increase the total solutions
        if (bricks == 0) {
            return 1;
        }
        // stop condition 2:
        // this is not a solution, because we couldn't divide bricks correctly
        // and we need more bricks to implement the solution,
        // therefore we return 0
        if (bricks < 0) {
            return 0;
        }
        // we call the same method for the 3 possible ways to unload bricks
        // we take the result of choosing every one of the 3 ways and we take the sum as the total
        // solution for current problem
        int count = 0;
        count += bricksToUnload(bricks - 1);
        count += bricksToUnload(bricks - 2);
        count += bricksToUnload(bricks - 3);
        // we return the total number of solutions
        return count;
    }
}
